//�ڿ���̨��ɫ��
//������<kerndev@foxmail.com>
#include <Windows.h>
#include "paint.h"

static HANDLE m_stdout;
static CONSOLE_SCREEN_BUFFER_INFO m_csbi;

void begin_paint(void)
{
	m_stdout = GetStdHandle(STD_OUTPUT_HANDLE);
	GetConsoleScreenBufferInfo(m_stdout, &m_csbi);
}

void end_paint(void)
{
	SetConsoleTextAttribute(m_stdout, m_csbi.wAttributes);
}

void draw_block(int color)
{
	WORD attr;
	attr = color ? (BACKGROUND_BLUE | BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_INTENSITY) : 0;
	SetConsoleTextAttribute(m_stdout, attr);
	WriteConsoleA(m_stdout, "  ", 2, NULL, NULL);
}

void new_line(void)
{
	WriteConsoleA(m_stdout, "\r\n", 2, NULL, NULL);
}
