#ifndef __QRENCODE_H
#define __QRENCODE_H

struct qrcode_bitmap
{
	int line_size;
	int data_size;
	unsigned char *data;
};

int qrcode_encode(unsigned char *data, int size, struct qrcode_bitmap *bitmap);

#endif
