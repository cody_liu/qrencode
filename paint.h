#ifndef __PAINT_H
#define __PAINT_H

void begin_paint(void);
void end_paint(void);
void draw_block(int color);
void new_line(void);

#endif
